import mockAxios from 'jest-mock-axios';
import { AxiosResponse } from 'axios';
import { Form2BotStoreBase, updateForm, sendForm } from '../refluxStore';
import { getRandomValue, getRandomString } from './utils';
import { encodeValue } from '../index';

describe('Testing Form2BotStoreBase functionality', () => {
  const testKey = getRandomString(16);
  const testValue = getRandomValue();
  const backendUrl = `https://${getRandomString(16)}.com`;

  class TestForm2BotStore extends Form2BotStoreBase {
    constructor() {
      super(
        {
          [testKey]: testValue,
        },
        backendUrl
      );
    }

    reset = () => this.setState({ [testKey]: testValue });
  }

  const store = new TestForm2BotStore();

  beforeEach(() => {
    mockAxios.reset();
    store.reset();
  });

  test('Extending Form2BotStoreBase test', () => {
    expect(store.state[testKey]).toBe(testValue);
    expect(store.state.isSending).toBe(false);
    expect(store.state.backendUrl).toBe(backendUrl);
  });

  test('updateForm action test', () => {
    expect(store.state[testKey]).toBe(testValue);

    const anotherTestValue = getRandomValue();
    updateForm(testKey, anotherTestValue);
    expect(store.state[testKey]).toBe(anotherTestValue);

    // Test reserved keywords
    expect(() => updateForm('isSending', getRandomValue())).toThrowError();
    expect(() => updateForm('backendUrl', getRandomValue())).toThrowError();
  });

  test('sendForm action success test', done => {
    const formBody = `${testKey}=${encodeValue(testValue)}`;
    mockAxios.post.mockResolvedValueOnce({ data: formBody, status: 200 });
    sendForm(
      (response: AxiosResponse) => {
        expect(store.state.isSending).toBeFalsy();
        expect(mockAxios.post).toHaveBeenCalledWith(backendUrl, formBody, {
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        });
        expect(response.status).toBe(200);
        expect(response.data).toBe(formBody);
        done();
      },
      (reason: string) => done.fail(reason)
    );

    expect(store.state.isSending).toBeTruthy();
  });

  test('sendForm action error test', done => {
    const formBody = `${testKey}=${encodeValue(testValue)}`;
    mockAxios.post.mockRejectedValueOnce({ data: 'Bad Gateway', status: 502 });
    sendForm(
      (response: AxiosResponse) => {
        done.fail('Method success should not be called');
      },
      (reason: string) => {
        expect(store.state.isSending).toBeFalsy();
        expect(mockAxios.post).toHaveBeenCalledWith(backendUrl, formBody, {
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        });
        done();
      }
    );

    expect(store.state.isSending).toBeTruthy();
  });
});

import { encodeValue, formDataToString, sendFormData } from '../index';
import mockAxios from 'jest-mock-axios';
import { getRandomString, getRandomNumber, getRandomValue } from './utils';

describe('Testing core functionality', () => {
  beforeEach(() => {
    mockAxios.reset();
  });

  test('encodeValue test', () => {
    const testString = getRandomString(32);
    const testNumber = getRandomNumber();
    expect(encodeValue(testString)).toBe(testString.toString());
    expect(encodeValue(testNumber)).toBe(testNumber.toString(10));
    expect(encodeValue(false)).toBe('0');
    expect(encodeValue(true)).toBe('1');
  });

  test('formDataToString test', () => {
    expect(formDataToString({})).toBe('');

    const formData = Object();
    for (let i = 0; i < 5; i++) {
      formData[`key${i.toString(10)}`] = getRandomValue();
    }

    let formBody = '';
    for (let i = 0; i < 5; i++) {
      if (i > 0) {
        formBody += '&';
      }

      const key = `key${i.toString(10)}`;
      formBody += `${key}=${encodeValue(formData[key])}`;
    }

    expect(formDataToString(formData)).toBe(formBody);
  });

  test('sendFormData test', done => {
    const backendUrl = `https://${getRandomString(16)}.com`;
    const formData = Object();
    for (let i = 0; i < 5; i++) {
      formData[`key${i.toString(10)}`] = getRandomValue();
    }
    const formBody = formDataToString(formData);

    mockAxios.post.mockResolvedValueOnce({ data: formBody, status: 200 });

    sendFormData(formData, backendUrl)
      .then(response => {
        expect(mockAxios.post).toHaveBeenCalledWith(backendUrl, formBody, {
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        });
        expect(response.status).toBe(200);
        expect(response.data).toBe(formBody);
        done();
      })
      .catch(reason => done.fail(reason));
  });
});

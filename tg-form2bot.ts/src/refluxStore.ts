import { AxiosResponse } from 'axios';
import { Store, createAction } from 'reflux';
import { FormData, sendFormData } from './index';

/**
 * Updates form key with value.
 * @param key form key as a string.
 * @param value value for this key as string, number or boolean.
 */
export const updateForm = createAction('updateForm');

/**
 * Sends form data as POST request.
 * @param success will be called on success with AxiosResponse.
 * @param error will called on fail with fail reason as string.
 */
export const sendForm = createAction('sendForm');

export interface Form2BotStoreState extends FormData {
  isSending: boolean;
  backendUrl: string;
}

export class Form2BotStoreBase extends Store {
  state: Form2BotStoreState;

  constructor(state: FormData, backendUrl: string) {
    super();

    this.state = {
      isSending: false,
      backendUrl,
    };
    Object.assign(this.state, state);

    this.listenTo(updateForm, this.onFormUpdate);
    this.listenTo(sendForm, this.onFormSend);
  }

  private onFormUpdate(key: string, value: string | number | boolean) {
    if (key === 'isSending' || key === 'backendUrl') {
      throw Error('Keys isSending and backendUrl cannot be used!');
    }

    this.setState({ [key]: value });
  }

  private onFormSend(
    success: (response: AxiosResponse) => void,
    error: (reason: string) => void
  ) {
    this.setState({ isSending: true });
    const formData = (({ isSending, backendUrl, ...data }) => ({ ...data }))(
      this.state
    );

    sendFormData(formData, this.state.backendUrl).then(
      response => {
        this.setState({ isSending: false });
        success(response);
      },
      errorReason => {
        this.setState({ isSending: false });
        error(errorReason);
      }
    );
  }
}
